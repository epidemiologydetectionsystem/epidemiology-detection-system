package com.epidemiology.detection.system.collector.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@NoArgsConstructor
@Data
@Entity
@Table(name = "EDS_TAG")
public class Tag implements Serializable
{
    private static final long serialVersionUID = -2590851897547580520L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    public String name;
}
