package com.epidemiology.detection.system.collector.repository;

import com.epidemiology.detection.system.collector.entity.Filter;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FilterRepository extends CrudRepository<Filter, Long>
{
    List<Filter> findAll();
    Filter findByName(String name);
}
