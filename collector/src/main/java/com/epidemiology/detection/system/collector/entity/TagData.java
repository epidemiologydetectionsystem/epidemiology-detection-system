package com.epidemiology.detection.system.collector.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.List;

@NoArgsConstructor
@Data
@Entity
@Table(name = "EDS_TAG_DATA")
public class TagData implements Serializable
{
    private static final long serialVersionUID = -6679799872367199919L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TAG_ID")
    private Tag tag;

    private ZonedDateTime timestamp;

    @OneToMany(mappedBy = "tagData", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Tweet> tweets;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "DATASET_ID")
    private Dataset dataset;
}
