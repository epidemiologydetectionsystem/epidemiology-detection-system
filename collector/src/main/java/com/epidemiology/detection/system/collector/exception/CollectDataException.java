package com.epidemiology.detection.system.collector.exception;

public class CollectDataException extends Exception
{
    private static final long serialVersionUID = 228688030830229849L;

    public CollectDataException()
    {
    }

    public CollectDataException(String message)
    {
        super(message);
    }

    public CollectDataException(String message, Throwable cause)
    {
        super(message, cause);
    }
}
