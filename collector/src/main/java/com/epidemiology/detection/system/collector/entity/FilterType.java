package com.epidemiology.detection.system.collector.entity;

public enum FilterType
{
    TAG("Tag"),
    WORD("Word");

    private String name;

    FilterType(String name)
    {
        this.name = name;
    }

    public String getName()
    {
        return name;
    }
}
