package com.epidemiology.detection.system.collector.exception;

public class DatasetException extends Exception
{
    private static final long serialVersionUID = -3705413270943084609L;

    public DatasetException()
    {
    }

    public DatasetException(String message)
    {
        super(message);
    }

    public DatasetException(String message, Throwable cause)
    {
        super(message, cause);
    }
}
