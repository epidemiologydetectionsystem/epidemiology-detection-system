package com.epidemiology.detection.system.collector.entity;

import lombok.AllArgsConstructor;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@AllArgsConstructor
@Data
@Entity
@Table(name = "EDS_Tweet")
public class Tweet implements Serializable
{
    private static final long serialVersionUID = 5083804874531291448L;

    @Id
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TAG_DATA_ID")
    private TagData tagData;

    private Date createdDate;
    private long userId;
    private String user;
    private String text;
}
