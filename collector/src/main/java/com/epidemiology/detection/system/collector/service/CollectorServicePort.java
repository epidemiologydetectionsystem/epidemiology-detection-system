package com.epidemiology.detection.system.collector.service;

import com.epidemiology.detection.system.collector.entity.Dataset;
import com.epidemiology.detection.system.collector.entity.Filter;
import com.epidemiology.detection.system.collector.entity.Tag;
import com.epidemiology.detection.system.collector.entity.Tweet;
import com.epidemiology.detection.system.collector.exception.*;

import java.util.List;

/**
 * Service to access twitter client
 */
public interface CollectorServicePort
{
    /**
     * Search tweets by hashtag
     *
     * @param hashtag hash tag
     * @return list of tweets
     * @throws SearchParametersException when an invalid hash tag is used
     */
    List<Tweet> searchByHashtag(String hashtag) throws SearchParametersException;

    /**
     * Create new tag
     *
     * @param name tag name
     */
    void createTag(String name) throws TagException;

    /**
     * Get all tags
     *
     * @return tags list
     */
    List<Tag> getAllTags();

    /**
     * Search for tags with partcial tag name
     *
     * @param parcialTag parcial tag name
     * @return tags list
     */
    List<Tag> searchTags(String parcialTag);

    /**
     * Get all filters
     *
     * @return filters list
     */
    List<Filter> getAllFilters();

    /**
     * Create new filter
     *
     * @param name filter name
     * @param type filter type
     * @param restriction filter restriction
     * @param word filter word
     * @param tag filter tag
     * @throws TagException when an invalid filter parameter is used
     */
    void createFilter(String name, String type, String restriction, String word, String tag) throws FilterException;

    /**
     * Get all datasets
     *
     * @return datasets list
     */
    List<Dataset> getAllDatasets();

    /**
     * Create dataset
     *
     * @param name dataset name
     * @param tagId tag id
     * @param filterId filter id
     * @throws DatasetException when an invalid dataset parameter is used
     */
    void createDataset(String name, String tagId, String filterId) throws DatasetException;

    /**
     * Get a dataset
     *
     * @param datasetId dataset id
     * @return dataset
     * @throws CollectDataException when a invalid dataset id is used
     */
    Dataset getDataset(long datasetId) throws CollectDataException;
}
