package com.epidemiology.detection.system.collector.exception;

public class SearchParametersException extends CollectorBaseException
{
    public SearchParametersException()
    {
    }

    public SearchParametersException(String message)
    {
        super(message);
    }

    public SearchParametersException(String message, Throwable cause)
    {
        super(message, cause);
    }
}
