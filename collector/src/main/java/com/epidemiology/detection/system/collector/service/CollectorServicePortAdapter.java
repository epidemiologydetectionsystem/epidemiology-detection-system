package com.epidemiology.detection.system.collector.service;

import com.epidemiology.detection.system.collector.client.TwitterClient;
import com.epidemiology.detection.system.collector.entity.*;
import com.epidemiology.detection.system.collector.exception.*;
import com.epidemiology.detection.system.collector.repository.DatasetRepository;
import com.epidemiology.detection.system.collector.repository.FilterRepository;
import com.epidemiology.detection.system.collector.repository.TagRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class CollectorServicePortAdapter implements CollectorServicePort
{
    private TwitterClient twitterClient;

    private TagRepository tagRepository;

    private FilterRepository filterRepository;

    private DatasetRepository datasetRepository;

    @Autowired
    public CollectorServicePortAdapter(TwitterClient twitterClient, TagRepository tagRepository, FilterRepository filterRepository,
                                       DatasetRepository datasetRepository)
    {
        this.twitterClient = twitterClient;
        this.tagRepository = tagRepository;
        this.filterRepository = filterRepository;
        this.datasetRepository = datasetRepository;
    }

    @Override
    public List<Tweet> searchByHashtag(String hashtag) throws SearchParametersException
    {
        return twitterClient.searchByHashtag(hashtag);
    }

    @Override
    public void createTag(String name) throws TagException
    {
        if (name == null || name.length() <= 0)
        {
            throw new TagException("Invalid name!");
        }

        Tag tag = new Tag();
        tag.setName(name);

        tagRepository.save(tag);
    }

    @Override
    public List<Tag> getAllTags()
    {
        return tagRepository.findAll();
    }

    @Override
    public List<Tag> searchTags(String parcialTag)
    {
        return tagRepository.findByParcialName(parcialTag);
    }

    @Override
    public List<Filter> getAllFilters()
    {
        return filterRepository.findAll();
    }

    @Override
    public void createFilter(String name, String type, String restriction, String word, String tag) throws FilterException
    {
        if (name.isBlank())
        {
            throw new FilterException("Empty name!");
        }

        //TODO check duplicate entries
        Optional<FilterType> optionalFilterType = Optional.of(FilterType.valueOf(type));
        FilterType filterType = optionalFilterType.orElseThrow(() -> new FilterException("Invalid type!"));

        Optional<FilterRestriction> optionalFilterRestriction = Optional.of(FilterRestriction.valueOf(restriction));
        FilterRestriction filterRestriction = optionalFilterRestriction.orElseThrow(() -> new FilterException("Invalid restriction!"));

        switch (filterType)
        {
            case TAG:
                if (tag.isBlank()) throw new FilterException("Tag filter requires a hashtag!");
                break;
            case WORD:
                if (word.isBlank()) throw new FilterException("Word filter requires a word!");
                break;
            default:
                throw new FilterException("Unknown filter type!");
        }

        Filter filter = new Filter();
        filter.setName(name);
        filter.setRestriction(filterRestriction);
        setWordTag(filter, filterType, tag, word);

        filterRepository.save(filter);
    }

    @Override
    public List<Dataset> getAllDatasets()
    {
        return datasetRepository.findAll();
    }

    @Override
    public void createDataset(String name, String tagId, String filterId) throws DatasetException
    {
        if (name.isBlank() || tagId.isBlank() || filterId.isBlank())
        {
            throw new DatasetException("Empty parameters!");
        }

        Filter filter = filterRepository.findById(Long.parseLong(filterId)).orElseThrow(() -> new DatasetException("Invalid filter!"));
        Tag tag = tagRepository.findById(Long.parseLong(tagId)).orElseThrow(() -> new DatasetException("Invalid tag!"));

        Dataset dataset = new Dataset();
        dataset.setName(name);
        dataset.setTag(tag);

        List<Filter> filters = new ArrayList<>();
        filters.add(filter);
        dataset.setFilter(filters);

        datasetRepository.save(dataset);
    }

    @Override
    public Dataset getDataset(long datasetId) throws CollectDataException
    {
        return datasetRepository.findById(datasetId).orElseThrow(() -> new CollectDataException("Invalid dataset id!"));
    }

    /**
     * Set the type of filter and tag or word according to the filter type
     *
     * @param filter filter
     * @param type filter type
     * @param tag filter tag
     * @param word filter word
     * @throws FilterException when a tag does not exist in the database
     */
    private void setWordTag(Filter filter, FilterType type, String tag, String word) throws FilterException
    {
        filter.setType(type);

        if (type == FilterType.TAG)
        {
            Tag dbTag = tagRepository.findByName(tag);

            if (dbTag == null)
            {
                throw new FilterException("Hashtag does not exist in the database!");
            }

            filter.setTag(dbTag);
            return;
        }

        filter.setWord(word);
    }
}
