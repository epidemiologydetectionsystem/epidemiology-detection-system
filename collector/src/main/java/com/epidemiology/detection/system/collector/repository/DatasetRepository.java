package com.epidemiology.detection.system.collector.repository;

import com.epidemiology.detection.system.collector.entity.Dataset;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DatasetRepository extends CrudRepository<Dataset, Long>
{
    List<Dataset> findAll();
}
