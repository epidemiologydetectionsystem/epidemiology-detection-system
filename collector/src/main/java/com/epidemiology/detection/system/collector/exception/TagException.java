package com.epidemiology.detection.system.collector.exception;

public class TagException extends CollectorBaseException
{
    public TagException()
    {
    }

    public TagException(String message)
    {
        super(message);
    }

    public TagException(String message, Throwable cause)
    {
        super(message, cause);
    }
}
