package com.epidemiology.detection.system.collector.client;

import com.epidemiology.detection.system.collector.entity.Tweet;
import com.epidemiology.detection.system.collector.exception.SearchParametersException;
import com.epidemiology.detection.system.collector.util.TwitterUtil;
import org.springframework.social.twitter.api.impl.TwitterTemplate;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class TwitterClient
{
    private TwitterTemplate twitterTemplate;

    public TwitterClient(TwitterTemplate twitterTemplate)
    {
        this.twitterTemplate = twitterTemplate;
    }

    public List<Tweet> searchByHashtag(String hashtag) throws SearchParametersException
    {
        if (hashtag == null || hashtag.length() <= 0)
        {
            throw new SearchParametersException("Invalid hashtag!");
        }

        return twitterTemplate.searchOperations()
                .search(hashtag)
                .getTweets()
                .stream()
                .map(tweet -> TwitterUtil.getInstance().mapTweet(tweet))
                .collect(Collectors.toList());
    }
}
