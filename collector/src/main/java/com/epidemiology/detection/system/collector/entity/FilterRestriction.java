package com.epidemiology.detection.system.collector.entity;

public enum FilterRestriction
{
    INCLUDE("Include"),
    EXCLUDE("Exclude");

    private String designation;

    FilterRestriction(String designation)
    {
        this.designation = designation;
    }

    public String getDesignation()
    {
        return designation;
    }
}
