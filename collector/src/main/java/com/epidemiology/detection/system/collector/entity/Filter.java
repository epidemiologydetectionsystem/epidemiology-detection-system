package com.epidemiology.detection.system.collector.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@NoArgsConstructor
@Data
@Entity
@Table(name = "EDS_FILTER")
public class Filter implements Serializable
{
    private static final long serialVersionUID = 7781080337606136058L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TAG_ID")
    private Tag tag;

    @Enumerated(EnumType.STRING)
    private FilterType type;

    @Enumerated(EnumType.STRING)
    private FilterRestriction restriction;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "DATASET_ID")
    private Dataset dataset;

    private String word;
    private String name;
}
