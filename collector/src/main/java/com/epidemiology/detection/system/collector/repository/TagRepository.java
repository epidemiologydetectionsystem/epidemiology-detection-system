package com.epidemiology.detection.system.collector.repository;

import com.epidemiology.detection.system.collector.entity.Tag;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TagRepository extends CrudRepository<Tag, Long>
{
    List<Tag> findAll();

    Tag findByName(String name);

    @Query("SELECT t FROM Tag t WHERE t.name LIKE '%?1%' ORDER BY name")
    List<Tag> findByParcialName(String name);
}
