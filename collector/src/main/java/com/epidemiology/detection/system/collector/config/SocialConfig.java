package com.epidemiology.detection.system.collector.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.social.connect.ConnectionFactoryLocator;
import org.springframework.social.connect.support.ConnectionFactoryRegistry;
import org.springframework.social.twitter.api.impl.TwitterTemplate;
import org.springframework.social.twitter.connect.TwitterConnectionFactory;

import java.util.Objects;

@Configuration
@EnableConfigurationProperties
@ConfigurationProperties(prefix = "twitter")
@Data
public class SocialConfig
{
    private String consumerKey;
    private String consumerSecret;
    private String accessToken;
    private String accessTokenSecret;

    @Bean
    public ConnectionFactoryLocator connectionFactoryLocator()
    {
        ConnectionFactoryRegistry registry = new ConnectionFactoryRegistry();
        registry.addConnectionFactory(new TwitterConnectionFactory(
                Objects.requireNonNull(consumerKey),
                Objects.requireNonNull(consumerKey)));
        return registry;
    }

    @Bean
    public TwitterTemplate twitterTemplate()
    {
        return new TwitterTemplate(
                Objects.requireNonNull(consumerKey),
                Objects.requireNonNull(consumerSecret),
                Objects.requireNonNull(accessToken),
                Objects.requireNonNull(accessTokenSecret)
        );
    }
}
