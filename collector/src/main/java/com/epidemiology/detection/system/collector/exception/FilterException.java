package com.epidemiology.detection.system.collector.exception;

public class FilterException extends Exception
{
    private static final long serialVersionUID = 1157802923726727506L;

    public FilterException()
    {
    }

    public FilterException(String message)
    {
        super(message);
    }

    public FilterException(String message, Throwable cause)
    {
        super(message, cause);
    }
}
