package com.epidemiology.detection.system.collector.util;

import com.epidemiology.detection.system.collector.entity.Tweet;

public class TwitterUtil
{
    private static TwitterUtil instance;

    private TwitterUtil()
    {
    }

    public static TwitterUtil getInstance()
    {
        if(instance == null)
        {
            instance = new TwitterUtil();
        }

        return instance;
    }

    public Tweet mapTweet(org.springframework.social.twitter.api.Tweet tweet)
    {
        return new Tweet(tweet.getId(), null, tweet.getCreatedAt(), tweet.getFromUserId(), tweet.getFromUser(), tweet.getText());
    }
}
