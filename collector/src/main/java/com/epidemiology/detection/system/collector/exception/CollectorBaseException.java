package com.epidemiology.detection.system.collector.exception;

public class CollectorBaseException extends Exception
{
    public CollectorBaseException()
    {
    }

    public CollectorBaseException(String message)
    {
        super(message);
    }

    public CollectorBaseException(String message, Throwable cause)
    {
        super(message, cause);
    }
}
