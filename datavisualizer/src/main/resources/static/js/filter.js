$(document).ready(function(){

    $("#create").click(function(){

        var filterName = $("#filterName").val();
        var filterType = $("#filterType option:selected").val();
        var filterRestriction = $("#filterRestriction option:selected").val();
        var filterWord = $("#filterWord").val();
        var filterTag = $("#filterTag").val();

        if( !filterType.trim() || !filterRestriction.trim()){

            alert("Vazios");

        }
        else{

         $.ajax({
             type: "POST",
             contentType: "application/json",
             url: "/filter/create",
             dataType: 'html',
             data: JSON.stringify({
                "name": filterName,
                "type": filterType,
                "restriction": filterRestriction,
                "word": filterWord,
                "tag": filterTag
             }),
             success: function (response) {

                 $("#searchResult").html(response);

             },
             error: function (response) {

                 alert("Error creating filter! \n" + response.responseJSON.message);

             }
         })
        }

    });

});