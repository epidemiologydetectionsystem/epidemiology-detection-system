$(document).ready(function(){

    $("#create").click(function(){

        var datasetName = $("#datasetName").val();
        var tag = $("#tag option:selected").val();
        var filter = $("#filter option:selected").val();

        if( !datasetName.trim() || !tag.trim() || !filter.trim()){

            alert("Vazios");

        }
        else{

         $.ajax({
             type: "POST",
             contentType: "application/json",
             url: "/dataset/create",
             dataType: 'html',
             data: JSON.stringify({
                "name": datasetName,
                "tagId": tag,
                "filterId": filter
             }),
             success: function (response) {

                 $("#searchResult").html(response);

             },
             error: function (response) {

                 alert("Error creating filter! \n" + response.responseJSON.message);

             }
         })
        }

    });

});