$(document).ready(function(){

    $("#showData").click(function(){

        var datasetId = $("#datasetId").val();

        if( !datasetId.trim()){

            alert("Vazios");

        }
        else{

         $.ajax({
             type: "GET",
             contentType: "application/json",
             url: "/collectData",
             dataType: 'html',
             data: JSON.stringify({
                "datasetId": datasetId
             }),
             success: function (response) {

                 $("#searchResult").html(response);

             },
             error: function (response) {

                 alert("Error search data collector! \n" + response.responseJSON.message);

             }
         })
        }

    });

});