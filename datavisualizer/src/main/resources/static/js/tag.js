$(document).ready(function(){

    $("#create").click(function(){

        var tagName = $("#tagName").val();

        if( !tagName.trim() ){

            alert("Vazios");

        }
        else{

         $.ajax({
             type: "POST",
             contentType: "application/json",
             url: "/tag/create",
             dataType: 'html',
             data: JSON.stringify({ "tagName": tagName }),
             success: function (response) {

                 $("#searchResult").html(response);

             },
             error: function (response) {

                 alert("Error creating tag! \n" + response.responseJSON.message);

             }
         })
        }

    });

});