$(document).ready(function(){

    $("#search").click(function(){

        var hashtag = $("#hashtag").val();

        if( !hashtag.trim() ){

            alert("Vazios");

        }
        else{

         $.ajax({
             type: "POST",
             contentType: "application/json",
             url: "/collector/search",
             dataType: 'html',
             data: JSON.stringify({ "hashtag": hashtag }),
             success: function (response) {

                 $("#searchResult").html(response);

             },
             error: function (response) {

                 alert("Error loading client types! \n" + response.responseJSON.message);

             }
         })
        }

    });

});