package com.epidemiology.detection.system.datavisualizer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@ComponentScan(basePackages = "com.epidemiology")
@EnableJpaRepositories("com.epidemiology.detection.system.collector.repository")
@EntityScan("com.epidemiology.detection.system.collector.entity")
public class DatavisualizerApplication {

	public static void main(String[] args) {
		SpringApplication.run(DatavisualizerApplication.class, args);
	}

}
