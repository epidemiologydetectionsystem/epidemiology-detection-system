package com.epidemiology.detection.system.datavisualizer.request;

import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class DatasetRequest
{
    private String name;
    private String tagId;
    private String filterId;
}
