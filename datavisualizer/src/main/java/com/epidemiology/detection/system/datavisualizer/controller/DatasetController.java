package com.epidemiology.detection.system.datavisualizer.controller;

import com.epidemiology.detection.system.collector.exception.DatasetException;
import com.epidemiology.detection.system.collector.service.CollectorServicePort;
import com.epidemiology.detection.system.datavisualizer.request.DatasetRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/dataset")
public class DatasetController
{
    @Autowired
    private CollectorServicePort collectorService;

    @GetMapping
    public ModelAndView index()
    {
        ModelAndView view = new ModelAndView("dataset/index");

        view.addObject("datasets", collectorService.getAllDatasets());
        view.addObject("tags", collectorService.getAllTags());
        view.addObject("filters", collectorService.getAllFilters());

        return view;
    }

    @PostMapping("/create")
    public ModelAndView create(@RequestBody DatasetRequest request) throws DatasetException
    {
        ModelAndView view = new ModelAndView("dataset/index :: #searchResult");

        collectorService.createDataset(request.getName(), request.getFilterId(), request.getTagId());

        view.addObject("datasets", collectorService.getAllDatasets());

        return view;
    }
}
