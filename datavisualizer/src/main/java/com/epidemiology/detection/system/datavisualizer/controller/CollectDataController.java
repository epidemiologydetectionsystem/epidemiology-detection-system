package com.epidemiology.detection.system.datavisualizer.controller;

import com.epidemiology.detection.system.collector.exception.CollectDataException;
import com.epidemiology.detection.system.collector.service.CollectorServicePort;
import com.epidemiology.detection.system.datavisualizer.util.DatasetUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/collectData")
public class CollectDataController
{
    @Autowired
    private CollectorServicePort collectorService;

    @GetMapping
    public ModelAndView index(String name)
    {
        ModelAndView view = new ModelAndView("collectData/index");

        view.addObject("datasets", collectorService.getAllDatasets());

        return view;
    }

    @GetMapping("/{dataSetId}")
    public ModelAndView search(@PathVariable("dataSetId") String dataSetId) throws CollectDataException
    {
        ModelAndView view = new ModelAndView("collectData/index :: #searchResult");

        view.addObject("dataset", DatasetUtil.getInstance().datasetToResponse(collectorService.getDataset(Long.parseLong(dataSetId))));

        return view;
    }
}
