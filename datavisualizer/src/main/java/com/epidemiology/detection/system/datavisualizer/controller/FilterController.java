package com.epidemiology.detection.system.datavisualizer.controller;

import com.epidemiology.detection.system.collector.entity.FilterRestriction;
import com.epidemiology.detection.system.collector.entity.FilterType;
import com.epidemiology.detection.system.collector.exception.FilterException;
import com.epidemiology.detection.system.collector.service.CollectorServicePort;
import com.epidemiology.detection.system.datavisualizer.request.FilterRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/filter")
public class FilterController
{
    @Autowired
    private CollectorServicePort collectorService;

    @GetMapping
    public ModelAndView index()
    {
        ModelAndView view = new ModelAndView("filter/index");

        view.addObject("filters", collectorService.getAllFilters());
        view.addObject("filterTypes", FilterType.values());
        view.addObject("filterRestrictions", FilterRestriction.values());

        return view;
    }

    @PostMapping("/create")
    public ModelAndView create(@RequestBody FilterRequest request) throws FilterException
    {
        ModelAndView view = new ModelAndView("filter/index :: #searchResult");

        collectorService.createFilter(request.getName(), request.getType(), request.getRestriction(), request.getWord(), request.getTag());

        view.addObject("filters", collectorService.getAllFilters());

        return view;
    }
}
