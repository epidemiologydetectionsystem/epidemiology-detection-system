package com.epidemiology.detection.system.datavisualizer.response;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class DataResponse
{
    private long id;
    private String tagName;
    private long numberOfTweets;
}
