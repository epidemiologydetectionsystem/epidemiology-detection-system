package com.epidemiology.detection.system.datavisualizer.controller;

import com.epidemiology.detection.system.collector.exception.TagException;
import com.epidemiology.detection.system.collector.service.CollectorServicePort;
import com.epidemiology.detection.system.datavisualizer.request.TagRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/tag")
public class TagController
{
    @Autowired
    private CollectorServicePort collectorService;

    @GetMapping
    public ModelAndView index()
    {
        ModelAndView view = new ModelAndView("tag/index");

        view.addObject("tags", collectorService.getAllTags());

        return view;
    }

    @PostMapping("/create")
    public ModelAndView create(@RequestBody TagRequest request) throws TagException
    {
        ModelAndView view = new ModelAndView("tag/index :: #searchResult");

        collectorService.createTag(request.getTagName());

        view.addObject("tags", collectorService.getAllTags());

        return view;
    }
}
