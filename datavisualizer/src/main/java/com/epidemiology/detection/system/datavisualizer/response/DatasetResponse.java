package com.epidemiology.detection.system.datavisualizer.response;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;

@AllArgsConstructor
@Getter
public class DatasetResponse
{
    private long id;
    private String name;
    private List<DataResponse> dataList;
}
