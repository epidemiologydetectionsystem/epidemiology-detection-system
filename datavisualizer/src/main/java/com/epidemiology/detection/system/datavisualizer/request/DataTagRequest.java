package com.epidemiology.detection.system.datavisualizer.request;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

@NoArgsConstructor
@Data
public class DataTagRequest
{
    private String dataset;
    private List<TweetRequest> tweets;

    @NoArgsConstructor
    @Data
    private class TweetRequest
    {
        private Date createdDate;
        private long userId;
        private String user;
        private String text;
    }
}
