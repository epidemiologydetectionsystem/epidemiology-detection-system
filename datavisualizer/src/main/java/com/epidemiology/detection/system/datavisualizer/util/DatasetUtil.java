package com.epidemiology.detection.system.datavisualizer.util;

import com.epidemiology.detection.system.collector.entity.Dataset;
import com.epidemiology.detection.system.datavisualizer.response.DataResponse;
import com.epidemiology.detection.system.datavisualizer.response.DatasetResponse;

import java.util.stream.Collectors;

public class DatasetUtil
{
    private static DatasetUtil instance;

    private DatasetUtil()
    {
    }

    public static DatasetUtil getInstance()
    {
        if(instance == null)
        {
            instance = new DatasetUtil();
        }

        return instance;
    }

    public DatasetResponse datasetToResponse(Dataset dataset)
    {
        return new DatasetResponse(dataset.getId(),
                dataset.getName(),
                dataset.getData().stream()
                        .map(tagData -> new DataResponse(tagData.getId(), tagData.getTag().getName(), tagData.getTweets().size()))
                        .collect(Collectors.toList()));
    }
}
