package com.epidemiology.detection.system.datavisualizer.controller;

import com.epidemiology.detection.system.collector.exception.SearchParametersException;
import com.epidemiology.detection.system.collector.service.CollectorServicePort;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/collector")
public class CollectorController
{
    @Autowired
    private CollectorServicePort collectorService;

    @GetMapping
    public ModelAndView index()
    {
        return new ModelAndView("collector/index");
    }

    @PostMapping("/search")
    public ModelAndView search(@RequestBody String hashtag) throws SearchParametersException
    {
        ModelAndView view = new ModelAndView("collector/index :: #searchResult");

        view.addObject("tweets", collectorService.searchByHashtag(hashtag));

        return view;
    }

    @PostMapping("/collect")
    public ModelAndView collect(@RequestBody String hashtag) throws SearchParametersException
    {
        ModelAndView view = new ModelAndView("collector/index :: #searchResult");

        view.addObject("tweets", collectorService.searchByHashtag(hashtag));

        return view;
    }
}
