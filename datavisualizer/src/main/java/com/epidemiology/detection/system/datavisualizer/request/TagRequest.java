package com.epidemiology.detection.system.datavisualizer.request;

import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class TagRequest
{
    private String tagName;
}
