package com.epidemiology.detection.system.datavisualizer.request;

import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class FilterRequest
{
    private String name;
    private String type;
    private String restriction;
    private String word;
    private String tag;
}
