package com.epidemiology.detection.system.datavisualizer.exception;

public class ServiceException extends Exception
{
    private static final long serialVersionUID = 8350630560872156215L;
}
